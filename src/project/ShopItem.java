package project;

public class ShopItem {
	// Super-class for all items in shop, has sub-classes: foodItem, adultItem
	
	private String name;
	private double price;
	private int stock; // amount of item
	
	public ShopItem() {
		this.name = "pencil";
		this.price = 1.29;
		this.stock = 3;
	}
	
	public ShopItem(String n, double p, int s) {
		this.name = n;
		this.price = 1;
		setPrice(p);
		this.stock = 1;
		setStock(s);
	}
	
	public ShopItem(String n, double p) {
		this.name = n;
		this.price = 1;
		setPrice(p);
		this.stock = 1;
	}
	
	public String getName() {
		return name;
	}
	
	public double getPrice() {
		return price;
	}
	
	public int getStock() {
		return stock;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setPrice(double price) {
		if (price >= 0) {
			this.price = price;
		}
	}
	
	public void setStock(int stock) {
		if (stock >= 0) {
			this.stock = stock;
		}
	}
	
	@Override
	public String toString() {
		return "Name: " + this.name + 
				"     Price: " + String.valueOf(this.price) +
				"     Amount in Stock: " +String.valueOf(this.stock);
	}
	
	public boolean equals(ShopItem s) {
		// Not including stock, plan to use equals method to "combine" stocks of the same thing
		if (this.name.compareTo(s.getName()) < 0) {
			return false;
		} else if (this.price != s.getPrice()) {
			return false;
		} else {
			return true;
		}
	}
	
}