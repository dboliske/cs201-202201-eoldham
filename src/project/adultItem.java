package project;

public class adultItem extends ShopItem {
	// Sub-class of ShopItem, contains additional minAge (minimum age to purchase) variable, mayBuy method
	
	private int minAge;
	
	public adultItem() {
		super();
		minAge = 21;
	}
	
	public adultItem(String n, double p, int s, int m) {
		super(n,p,s);
		minAge = m;
	}
	
	public adultItem(String n, double p, int m) {
		super(n,p);
		minAge = m;
	}
	
	public int getMinAge() {
		return minAge;
	}
	
	public void setMinAge(int m) {
		this.minAge = m;
	}
	
	public boolean mayBuy(int age) {
		if (this.minAge <= age) {
			return true;
		} else {
			return false;
		}
	}
	
	@Override
	public String toString() {
		return super.toString() +
				"     Must be " + String.valueOf(this.minAge) + " years or older to purchase.";
	}
	
	// Not overriding equals method, age limit will be the same for any one type of item at any given time.
}