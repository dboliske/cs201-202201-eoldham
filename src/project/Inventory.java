package project;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class Inventory {

	private ArrayList<ShopItem> inv;
	
	public Inventory() {
		inv = new ArrayList<ShopItem>(0);
	}
	
	public Inventory(ArrayList<ShopItem> i) {
		inv = i;
	}
	
	public void setInv(ArrayList<ShopItem> i) {
		this.inv = i;
	}
	
	public ArrayList<ShopItem> getInv() {
		return inv; 
	}
	
	
	public void sort() {
		ArrayList<ShopItem> tempInv = inv;
		
		// Sorts
		for (int j=1; j<tempInv.size(); j++) {
			int i = j;
			while (i > 0 && tempInv.get(i).getName().compareTo(tempInv.get(i-1).getName()) < 0) {
				ShopItem tempItem = tempInv.get(i);
				tempInv.set(i, tempInv.get(i-1));
				tempInv.set(i-1, tempItem);
				i--;
			}
		}
		
		// Merges equal items
		for (int i = 0; i < tempInv.size(); i++) {
			for (int j = i+1; j < tempInv.size(); j++) {
				if (tempInv.get(i).equals(tempInv.get(j))) {
					tempInv.get(i).setStock(tempInv.get(i).getStock() + tempInv.get(j).getStock());
					tempInv.remove(j);
					j--;
				}
			}
		}
		
		
		this.inv = tempInv;
	}
	
	
	public int search(String value) {
		// Binary Search
		// Will only return ONE INDEX!!! Need to look around this index for other items of the same name.
		int start = 0;
		int end = this.inv.size();
		int pos = -1;
		boolean found = false;
		while (!found && start != end) {
			int middle = (start + end) / 2;
			if (this.inv.get(middle).getName().equalsIgnoreCase(value)) {
				found = true;
				pos = middle;
			} else if (this.inv.get(middle).getName().compareToIgnoreCase(value) < 0) {
				start = middle + 1;
			} else {
				end = middle;
			}
		}
		
		return pos;
	}
	
	public void loadInv(File f) {
		try {
			Scanner file = new Scanner(f);	
			while (file.hasNextLine()) {	
				String[] values = file.nextLine().split(",");
				String name = values[0];
				String priceStr = values[1];
				double price = Double.parseDouble(priceStr);
				
				if (values.length == 2) {
					this.inv.add(new ShopItem(name, price));
				} else if (values.length == 3) {
					if (values[2].contains("/")) {
						String[] date = values[2].split("/");
						int day = Integer.parseInt(date[1]);
						int month = Integer.parseInt(date[0]);
						int year = Integer.parseInt(date[2]);
						this.inv.add(new foodItem(name, price, day, month, year));
					} else {
						int minAge = Integer.parseInt(values[2]);
						this.inv.add(new adultItem(name, price, minAge));
					}
				}
			}
			
			file.close();
		} catch (FileNotFoundException e) {
			System.out.println("Error: File not found");
		}
	}
	
	
	public void saveInv(Scanner input) {
		try {
			System.out.print("Enter file path: "); // I made a file named Question2Text.txt for testing, it should be in the labs.lab3 package
			String filePath = input.nextLine();
			
			FileWriter fWriter = new FileWriter(filePath);
			
			for (int i = 0; i < inv.size(); i++) {
				for (int j = inv.get(i).getStock(); j>=0; j--) {
					fWriter.write(inv.get(i).getName() + "," + inv.get(i).getPrice());
					
					if (inv.get(i) instanceof foodItem) {
						fWriter.write("," + ((foodItem) inv.get(i)).getExpMonth() + "/" + ((foodItem) inv.get(i)).getExpDay() + "/" + ((foodItem) inv.get(i)).getExpYear());
					} else if (inv.get(i) instanceof adultItem) {
						fWriter.write("," + ((adultItem) inv.get(i)).getMinAge());
					}
					
					if ((i != inv.size()-1) || (j!=0)) {
						fWriter.write(System.getProperty("line.separator"));
					}
				}
			}
			
			fWriter.close();
			} catch (IOException e) {
				System.out.print("Error writing to file");
			}
	}
}