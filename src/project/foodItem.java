package project;

public class foodItem extends ShopItem{
	// Sub-class of ShopItem, contains additional expiration date variables, and isExp check
	
	private int day;
	private int month;
	private int year;
	
	public foodItem() {
		super();
		this.day = 29;
		this.month = 4;
		this.year = 2022;
	}
	
	public foodItem(String n, double p, int s, int d, int m, int y) {
		super(n,p,s);
		this.day = 1;
		setExpDay(d);
		this.month = 1;
		setExpMonth(m);
		this.year = 1;
		setExpYear(y);
	}
	
	public foodItem(String n, double p, int d, int m, int y) {
		super(n,p);
		this.day = 1;
		setExpDay(d);
		this.month = 1;
		setExpMonth(m);
		this.year = 1;
		setExpYear(y);
	}
	
	
	public int getExpDay() {
		return day;
	}
	
	public int getExpMonth() {
		return month;
	}
	
	public int getExpYear() {
		return year;
	}
	
	public void setExpDay(int d) {
		if ((d >=1) & (d <=31)) {
			this.day = d;
		}
	}
	
	public void setExpMonth(int d) {
		if ((d >=1) & (d <=12)) {
			this.month = d;
		}
	}
	
	public void setExpYear(int d) {
			this.year = d;
	}
	
	public boolean isExp(int d, int m, int y) {
		// false is not expired, true is expired
		
		if (this.year > y) {
			return false;
		} else if ((this.month > m) & (this.year == y)) {
			return false;
		} else if ((this.day > d) & (this.month == m) & (this.year == y)) {
			return false;
		}
		return true;
	}
	
	@Override
	public String toString() {
		 return super.toString() +
				 "     Expires on " + String.valueOf(this.month) + "/" + String.valueOf(this.day) + "/" + String.valueOf(this.year) + " (m/d/y)";
	}
	
	@Override
	public boolean equals(ShopItem f) {
		if (!super.equals(f)) {
			return false;
		} else if (!(f instanceof foodItem)) {
			return false;
		}
		
		foodItem food = (foodItem)f;
		if (this.day != food.getExpDay()) {
			return false;
		} else if (this.month != food.getExpMonth()) {
			return false;
		} else if (this.year != food.getExpYear()) {
			return false;
		}
		
		return true;
	}
}