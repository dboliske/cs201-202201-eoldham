package project;

import java.io.File;
import java.util.ArrayList;
import java.util.Scanner;

public class ShopUI {
	
	 
	
	// 1) Add/Remove from stock
	
	public static void restock(Inventory inv, int pos, int add) {
			ArrayList<ShopItem> tempInv = inv.getInv();
			tempInv.get(pos).setStock(tempInv.get(pos).getStock() + add);
			
			inv.setInv(tempInv);
	}
	
	public static void restockEval(String itemString, String addString, Inventory inv) {
		boolean hasParsed = true;
		int add = 0;
		int item = 0;
		
		try {
			item = Integer.parseInt(itemString);
			add = Integer.parseInt(addString);
		} catch (NumberFormatException e) {
			System.out.println("Not a valid integer");
			hasParsed = false;
		}
		
		if (hasParsed) {
			if ((0 <= item) && (item < inv.getInv().size())) {
				int prevStock = inv.getInv().get(item).getStock();
				restock(inv, item, add);
			
			
				if ((add != 0) && (inv.getInv().get(item).getStock() == prevStock)) {
					System.out.println("Not enough items in stock to perform this action.");
				} else {
					System.out.println(add + " items addedd to stock of " + inv.getInv().get(item).toString() + ".");
				}
			} else {
				System.out.println(item + " is not a valid index.");
			}
		}
	}
	
	
	// 2) Add new
	
	public static void addNew(Scanner input, Inventory inv) {
		System.out.print("Enter name of item: ");
		String name = input.nextLine();
		System.out.print("Enter price of item: ");
		String priceStr = input.nextLine();
		System.out.print("Enter item stock: ");
		String stockStr = input.nextLine();
		
		boolean hasParsed = true;
		double price = -1;
		int stock = -1;
		
		try {
			price = Integer.parseInt(priceStr);
			stock = Integer.parseInt(stockStr);
		} catch (NumberFormatException e) {
			System.out.println("Not a valid input");
			hasParsed = false;
		}
		
		if (hasParsed) {
			if (price < 0) {
				System.out.println("Negative prices not allowed");
			} else if (stock < 0) {
				System.out.println("Negative stock not allowed");
			} else {
				inv.getInv().add(new ShopItem(name, price, stock));
				System.out.println("Added:");
				System.out.println(inv.getInv().get(inv.getInv().size() -1).toString());
				System.out.println("Add an expiration date, or minimum age to purchace in the modify tab.");
			}
		}
	}
	
	
	// 3) Delete All
	
	public static void delAll(String posStr, Inventory inv) {
		int pos = 0;
		boolean hasParsed = true;
		try {
			pos = Integer.parseInt(posStr);
		} catch (NumberFormatException e) {
			System.out.println(posStr + " is not a valid integer");
			hasParsed = false;
		}
		
		if (hasParsed) {
			if ((pos < 0) || (pos >= inv.getInv().size())) {
				System.out.println(pos + " is not a valid index");
			} else {
				System.out.println("Removing all instances of:    " + inv.getInv().get(pos).toString());
				inv.getInv().remove(pos);
			}
		}
	}
	
	
	// 4) Sell Item
	
	public static void checkOut(int [] cart, Inventory inv) {
		System.out.println("Checked out for a total cost of: $" + getCost(cart, inv));
		for (int i = 0; i<cart.length; i++) {
			if (cart[i] != 0) {
				inv.getInv().get(i).setStock(inv.getInv().get(i).getStock() - cart[i]);
			}
		}
	}
	
	public static double getCost(int[] cart, Inventory inv) {
		double cost = 0;
		for (int i = 0; i<cart.length; i++) {
			if (cart[i] != 0) {
				cost = cost + (((double) cart[i])*inv.getInv().get(i).getPrice());
			}
		}
		return cost;
	}
	
	public static void dispCart(int[] cart, Inventory inv) {
		System.out.println("Cart: ");
		for (int i = 0; i<cart.length; i++) {
			if (cart[i] != 0) {
				System.out.println(inv.getInv().get(i).getName() + ", ($" + inv.getInv().get(i).getPrice() + "), Quantity: " + cart[i] + ", Index: " + i);
			}
		}
		
	}
	
	public static int[] addByIndex(int[] cart, Scanner input, Inventory inv) {
		System.out.print("Enter item index: ");
		String indexStr = input.nextLine();
		System.out.print("Enter amount to add to cart: ");
		String amountStr = input.nextLine();
		
		int pos = -1;
		int amount = -1;
		boolean hasParsed = true;
		
		try {
			pos = Integer.parseInt(indexStr);
			amount = Integer.parseInt(amountStr);
		} catch (NumberFormatException e) {
			System.out.println("Not a valid integer");
			hasParsed = false;
		}
		
		if (hasParsed) {
			if ((pos < 0) || (pos >= inv.getInv().size())) {
				System.out.println(pos + " is not a vaild index");
			} else if ((amount < 0) || (amount > inv.getInv().get(pos).getStock())) {
				System.out.println(amount + " is not a vaild amount");
			} else {
				cart[pos] = amount;
				System.out.println("Added " + amount + " " + inv.getInv().get(pos).getName() + " ($" + inv.getInv().get(pos).getPrice() + " per item)" + "to cart" );
				return cart;
			}
		}
		return cart;
	}
	
	public static void sell(Scanner input, Inventory inv) {
		boolean flag = false;
		int[] cart = new int[inv.getInv().size()];
		do {
			System.out.println("Options:");
			System.out.println("1) Add item to cart by index");
			System.out.println("2) Search for item to add to cart");
			System.out.println("3) Remove from cart");
			System.out.println("4) Check-out");
			System.out.println("5) Exit to menu");
			System.out.print("Enter number of your choice: ");
			String choice = input.nextLine();
			
			switch (choice) {
			case "1": // Add by index
				cart = addByIndex(cart, input, inv);
				break;
			case "2": // Search and add
				System.out.print("Enter item name: ");
				String name = input.nextLine();
				
				searchName(name, inv);
				cart = addByIndex(cart, input, inv);
				break;
			case "3": // Remove from cart
				dispCart(cart, inv);
				
				System.out.print("Enter index of item you want to remove: ");
				String indexStr = input.nextLine();
				
				int pos = -1;
				boolean hasParsed = true;
				
				try {
					pos = Integer.parseInt(indexStr);
				} catch (NumberFormatException e) {
					System.out.println(indexStr + " is not a valid integer");
					hasParsed = false;
				}
				
				if (hasParsed) {
					if ((pos < 0) || (pos >= inv.getInv().size())) {
						System.out.println(pos + " is not a valid index");
					} else {
						cart[pos] = 0;
						System.out.println(inv.getInv().get(pos).getName() + " removed from cart");
					}
				}
				break;
			case "4": // Check-out
				dispCart(cart, inv);
				double cost = getCost(cart, inv);
				System.out.println("Total price: $" + cost);
				
				boolean flag2 = false;
				do {
				System.out.print("Do you want to check out? (yes/no): ");
				String choice2 = input.nextLine().toLowerCase();
				
				switch (choice2) {
					case "y":
					case "yes":
						checkOut(cart, inv);
						cart = new int[inv.getInv().size()];
						flag2 = true;
						break;
					case "n":
					case "no":
						flag2 = true;
						break;
					default:
						System.out.println(choice2 + " is not a valid answer");
				}
				
				} while (!flag2);
				
				break;
			case "5": // Exit
				flag = true;
				break;
			default:
				System.out.println(choice + " is not a valid option");
			}
			
		} while (!flag);
	}
	
	
	// 5) Search for an item
	
	public static void searchName(String name, Inventory inv) {
		int pos1 = inv.search(name);
		
		if (pos1 == -1) {
			System.out.println(name + " not found in inventory.");
		} else {
			int[] allPos = new int[0];
			int[] tempAllPos = new int[0];
			
			// Gets indices of all Items with the same name
			do {
				tempAllPos = new int[allPos.length+1];
				for (int i = 0; i<allPos.length; i++) {
					tempAllPos[i] = allPos[i];
				}
				tempAllPos[allPos.length] = pos1;
				allPos = tempAllPos;
				
				inv.getInv().get(pos1).setName(inv.getInv().get(pos1).getName() + " - Searched"); // Change names so they wont be searched again
				
				pos1 = inv.search(name);
			} while (pos1 != -1);
			
			// Change names back
			for (int i = 0; i<allPos.length; i++) {
				inv.getInv().get(allPos[i]).setName(name.toLowerCase());
			}
			
			System.out.println("The following items have the same name:");
			for (int i = 0; i<allPos.length; i++) {
				System.out.println("");
				System.out.println(inv.getInv().get(allPos[i]).toString() + "     Index: " + allPos[i]);
			}
		}
	}
	
	// 6) Modify item
	
	public static void modify(Inventory inv, Scanner input) {
		System.out.print("Enter item index: ");
		String indexMod = input.nextLine();
		
		int pos = 0;
		boolean hasParsed = true;
		
		try {
			pos = Integer.parseInt(indexMod);
		} catch (NumberFormatException e) {
			System.out.println(indexMod + " is not a valid integer");
			hasParsed = false;
		}
		
		if (hasParsed) {
			if ((pos >= 0) && (pos < inv.getInv().size())) {
				System.out.println("Item ----->   " + inv.getInv().get(pos).toString());
				ShopItem item = inv.getInv().get(pos);
				
				boolean flag = false;
				do {
					String choiceMod = "";
					if (item != null) {
						item = inv.getInv().get(pos);
						if (item instanceof foodItem) {
							System.out.println("What value do you want to change?");
							System.out.println("1) Name");
							System.out.println("2) Price");
							System.out.println("3) Day");
							System.out.println("4) Month");
							System.out.println("5) Year");
							System.out.println("6) Remove expiration date");
							System.out.println("7) Return to menu");
							System.out.print("Choice: ");
							choiceMod = input.nextLine();
							flag = modifyFood(choiceMod, pos, inv, input);
						} else if (item instanceof adultItem) {
							System.out.println("What value do you want to change?");
							System.out.println("1) Name");
							System.out.println("2) Price");
							System.out.println("3) Minumum Age to purchase");
							System.out.println("4) Remove minimum age to purchase");
							System.out.println("5) Return to menu");
							System.out.print("Choice: ");
							choiceMod = input.nextLine();
							flag = modifyAdult(choiceMod, pos, inv, input);
						} else {
							System.out.println("What value do you want to change?");
							System.out.println("1) Name");
							System.out.println("2) Price");
							System.out.println("3) Add Expiration Date");
							System.out.println("4) Add Minimum age to purchace");
							System.out.println("5) Return to menu");
							System.out.print("Choice: ");
							choiceMod = input.nextLine();
							flag = modifyGeneral(choiceMod, pos, inv, input);
						}
					}
					System.out.println("Item ----->   " + inv.getInv().get(pos).toString());
					
				} while (!flag);
				
			} else {
				System.out.println(pos + " is not a valid index.");
			}
		}
	}
	
	public static boolean modifyGeneral(String choice, int pos, Inventory inv, Scanner input) {
		switch (choice) {
		case "1": // Name
			System.out.print("Enter new name: ");
			String nameNew = input.nextLine();
			
			inv.getInv().get(pos).setName(nameNew);
			break;
		case "2": // Price
			System.out.print("Enter new price: ");
			String priceStr = input.nextLine();
			
			double price = 0;
			boolean hasParsed = true;
			
			try {
				price = Double.parseDouble(priceStr);
			} catch (NumberFormatException e) {
				System.out.println(priceStr + " is not a valid number");
				hasParsed = false;
			}
			
			if (hasParsed) {
				if (price < 0) {
					System.out.println("Negative prices not allowed");
				} else {
					inv.getInv().get(pos).setPrice(price);
				}
			}
			
			break;
		case "3": // Add Exp Date
			System.out.print("Enter day: ");
			String dayStr = input.nextLine();
			System.out.print("Enter month: ");
			String monthStr = input.nextLine();
			System.out.print("Enter year: ");
			String yearStr = input.nextLine();
			
			int day = -1;
			int month = -1;
			int year = -1;
			hasParsed = true;
			
			try {
				day = Integer.parseInt(dayStr);
				month = Integer.parseInt(monthStr);
				year = Integer.parseInt(yearStr);
			} catch (NumberFormatException e) {
				System.out.println("An input is not a valid number");
				hasParsed = false;
			}
			
			if (hasParsed) {
				if ((day<=0)||(day>31) ) {
					System.out.println(day + " is not a valid day");
				} else if ((month<=0) || (month>12)) {
					System.out.println(month + " is not a valid month");
				} else if (year<=0) {
					System.out.println(year + " is not a valid year");
				} else {
					inv.getInv().set(pos, new foodItem(inv.getInv().get(pos).getName(), inv.getInv().get(pos).getPrice(), inv.getInv().get(pos).getStock(), day, month, year));
				}
			}
			
			break;
		case "4": // Add Min Age
			System.out.print("Enter age: ");
			String ageStr = input.nextLine();
			
			int age = -1;
			hasParsed = true;
			
			try {
				age = Integer.parseInt(ageStr);
			} catch (NumberFormatException e) {
				System.out.println(ageStr + " is not a valid number");
				hasParsed = false;
			}
			
			if (hasParsed) {
				if (age<=0) {
					System.out.println(age + " is not a valid age");
				} else {
					inv.getInv().set(pos, new adultItem(inv.getInv().get(pos).getName(), inv.getInv().get(pos).getPrice(), inv.getInv().get(pos).getStock(), age));
				}
			}
			
			break;
		case "5": // Return to menu
			return true;
		default:
			System.out.println("Invalid Choice");
		}
		return false;
	}
	
	public static boolean modifyFood(String choice, int pos, Inventory inv, Scanner input) {
		boolean hasParsed = true;
		switch (choice) {
		case "1": // Name
			System.out.print("Enter new name: ");
			String nameNew = input.nextLine();
			
			inv.getInv().get(pos).setName(nameNew);
			break;
		case "2": // Price
			System.out.print("Enter new price: ");
			String priceStr = input.nextLine();
			
			double price = 0;
			hasParsed = true;
			
			try {
				price = Double.parseDouble(priceStr);
			} catch (NumberFormatException e) {
				System.out.println(priceStr + " is not a valid number");
				hasParsed = false;
			}
			
			if (hasParsed) {
				if (price < 0) {
					System.out.println("Negative prices not allowed");
				} else {
					inv.getInv().get(pos).setPrice(price);
				}
			}
			
			break;
		case "3": // Day
			System.out.print("Enter new day: ");
			String dayStr = input.nextLine();
			
			int day = -1;
			hasParsed = true;
			
			try {
				day = Integer.parseInt(dayStr);
			} catch (NumberFormatException e) {
				System.out.println(dayStr + " is not a valid number");
				hasParsed = false;
			}
			
			if (hasParsed) {
				if ((day<=0) || (day > 31)) {
					System.out.println(day + " is not a valid day");
				} else {
					((foodItem) inv.getInv().get(pos)).setExpDay(day);
				}
			}
			
			break;	
		case "4": // Month
			System.out.print("Enter new month: ");
			String monthStr = input.nextLine();
			
			int month = -1;
			hasParsed = true;
			
			try {
				month = Integer.parseInt(monthStr);
			} catch (NumberFormatException e) {
				System.out.println(monthStr + " is not a valid number");
				hasParsed = false;
			}
			
			if (hasParsed) {
				if ((month<=0) || (month > 12)) {
					System.out.println(month + " is not a valid month");
				} else {
					((foodItem) inv.getInv().get(pos)).setExpMonth(month);
				}
			}
			
			break;
		case "5": // Year
			System.out.print("Enter new year: ");
			String yearStr = input.nextLine();
			
			int year = -1;
			hasParsed = true;
			
			try {
				year = Integer.parseInt(yearStr);
			} catch (NumberFormatException e) {
				System.out.println(yearStr + " is not a valid number");
				hasParsed = false;
			}
			
			if (hasParsed) {
				if (year<=0) {
					System.out.println(year + " is not a valid year");
				} else {
					((foodItem) inv.getInv().get(pos)).setExpYear(year);
				}
			}
			break;
		case "6": // Remove Exp Date
			inv.getInv().set(pos, new ShopItem(inv.getInv().get(pos).getName(), inv.getInv().get(pos).getPrice(), inv.getInv().get(pos).getStock()));
			
			break;
		case "7": // Return to menu
			return true;
		default:
			System.out.println("Invalid Choice");
		}
		return false;
	}
	
	public static boolean modifyAdult(String choice, int pos, Inventory inv, Scanner input) {
		switch (choice) {
		case "1": // Name
			System.out.print("Enter new name: ");
			String nameNew = input.nextLine();
			
			inv.getInv().get(pos).setName(nameNew);
			break;
		case "2": // Price
			System.out.print("Enter new price: ");
			String priceStr = input.nextLine();
			
			double price = 0;
			boolean hasParsed = true;
			
			try {
				price = Double.parseDouble(priceStr);
			} catch (NumberFormatException e) {
				System.out.println(priceStr + " is not a valid number");
				hasParsed = false;
			}
			
			if (hasParsed) {
				if (price < 0) {
					System.out.println("Negative prices not allowed");
				} else {
					inv.getInv().get(pos).setPrice(price);
				}
			}
			
			break;
		case "3": // Min age
			System.out.print("Enter new age: ");
			String ageStr = input.nextLine();
			
			int age = -1;
			hasParsed = true;
			
			try {
				age = Integer.parseInt(ageStr);
			} catch (NumberFormatException e) {
				System.out.println(ageStr + " is not a valid number");
				hasParsed = false;
			}
			
			if (hasParsed) {
				if (age<=0) {
					System.out.println(age + " is not a valid age");
				} else {
					((adultItem) inv.getInv().get(pos)).setMinAge(age);
				}
			}
			
			break;
		case "4": // Remove min age
			inv.getInv().set(pos, new ShopItem(inv.getInv().get(pos).getName(), inv.getInv().get(pos).getPrice(), inv.getInv().get(pos).getStock()));
			
			break;
		case "5": // Return to menu
			return true;
		default:
			System.out.println("Invalid Choice");
		}
		return false;
	}
	
	// 9) Show exp
	
	public static void dispExp(Scanner input, Inventory inv) {
		System.out.print("Enter day: ");
		String dayStr = input.nextLine();
		System.out.print("Enter month: ");
		String monthStr = input.nextLine();
		System.out.print("Enter year: ");
		String yearStr = input.nextLine();
		
		int day = -1;
		int month = -1;
		int year = -1;
		boolean hasParsed = true;
		
		try {
			day = Integer.parseInt(dayStr);
			month = Integer.parseInt(monthStr);
			year = Integer.parseInt(yearStr);
		} catch (NumberFormatException e) {
			System.out.println("An input is not a valid number");
			hasParsed = false;
		}
		
		if (hasParsed) {
			if ((day<=0)||(day>31) ) {
				System.out.println(day + " is not a valid day");
			} else if ((month<=0) || (month>12)) {
				System.out.println(month + " is not a valid month");
			} else if (year<=0) {
				System.out.println(year + " is not a valid year");
			} else {
				for (int i = 0; i < inv.getInv().size(); i++) {
					if ((inv.getInv().get(i) instanceof foodItem) && (((foodItem) inv.getInv().get(i)).isExp(day,month,year))) {
						System.out.println("Index: " + i + " ----> " + inv.getInv().get(i).toString());
					}
				}
			}
		}
	}
	
	
	// 10) May buy
	
	public static void dispMayBuy(Scanner input, Inventory inv) {
		System.out.print("Enter age: ");
		String ageStr = input.nextLine();
		
		int age = -1;
		boolean hasParsed = true;
		
		try {
			age = Integer.parseInt(ageStr);
		} catch (NumberFormatException e) {
			System.out.println("An input is not a valid number");
			hasParsed = false;
		}
		
		if (hasParsed) {
			if (age<0) {
				System.out.println(age + " is not a valid age");
			} else {
				for (int i = 0; i < inv.getInv().size(); i++) {
					if (!(inv.getInv().get(i) instanceof adultItem)) {
						System.out.println("Index: " + i + " ----> " + inv.getInv().get(i).toString());
					} else if (((adultItem) inv.getInv().get(i)).mayBuy(age)) {
						System.out.println("Index: " + i + " ----> " + inv.getInv().get(i).toString());
					}
				}
			}
		}
	}
	
	
	
	
	
	
	
	//      //
	// MAIN //
	//      //
	public static void main(String[] args) {
		// User Interface for shop. Checks if shopper or admin/shop-keeper
		Scanner input = new Scanner(System.in);
		
		File f = new File("src/project/stock.csv");
		Inventory inv = new Inventory();
		
		inv.loadInv(f);
		inv.sort();
		
		
		boolean flag = false;
	
		do {
			System.out.println();
			System.out.println("Choose an option:");
			System.out.println("1) Add/Remove items from stock");
			System.out.println("2) Add New Instance of an Item");
			System.out.println("3) Delete all instances of an item");
			System.out.println("4) Sell Item");
			System.out.println("5) Search for an item");
			System.out.println("6) Modify item");
			System.out.println("7) Load new stock from file");
			System.out.println("8) Save stock to file");
			System.out.println("9) Display expired food");
			System.out.println("10) Display items that may be bought at specified age");
			System.out.println("11) Display inventory");
			System.out.println("12) Exit program");
			System.out.print("Choose a number: ");
			String choice = input.nextLine();
			
			switch (choice) {
			case "1": // Re-stock
				System.out.print("Enter item index: ");
				String itemString = input.nextLine();
				System.out.print("Enter amount to add (negative integer to remove): ");
				String addString = input.nextLine();
				
				restockEval(itemString, addString, inv);
				
				break;
			case "2":
				addNew(input, inv);
				inv.sort();
				
				break;
			case "3": // Delete all
				System.out.println("Warning, will delete all instances of item. Enter an invalid index (such as '-1') to return to menu without deleting anything.");
				System.out.print("Enter item index: ");
				String delStr = input.nextLine();
				
				delAll(delStr, inv);
				
				break;
			case "4": // Sell
				sell(input, inv);
				
				break;
			case "5": // Search
				System.out.println("Enter name of item to search: ");
				String nameSearch = input.nextLine();
				
				searchName(nameSearch, inv);
				
				break;
			case "6": // Modify
				modify(inv, input);
				inv.sort();
				break;
			case "7": // Load new
				System.out.print("Enter file path: ");
				String fileStr = input.nextLine();
				File f2 = new File(fileStr);
				
				inv = new Inventory();
				inv.loadInv(f2);
				inv.sort();
				
				break;
			case "8": // Save stock to file
				inv.saveInv(input);
				
				break;
			case "9": // Show expired
				dispExp(input, inv);
				
				break;
			case "10": // May buy?
				dispMayBuy(input, inv);
				
				break;
			case "11": // Display inventory
				System.out.println();
				for (int i = 0; i<inv.getInv().size(); i++) {
					System.out.println("Index: " + i + " ----> " + inv.getInv().get(i).toString());
				}
				break;
			case "12": // Exit
				flag = true;
				break;
			default:
				System.out.println("Invalid choice.");
			}
			
		} while (!flag);
		
		
		input.close();
	}
}