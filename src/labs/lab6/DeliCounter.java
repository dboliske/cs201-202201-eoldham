package labs.lab6;

import java.util.ArrayList;
import java.util.Scanner;

public class DeliCounter {

	private ArrayList<String> queue;
	
	public DeliCounter() {
		this.queue = new ArrayList<String>();
	}
	
	public DeliCounter(ArrayList<String> q) {
		this.queue = q;
	}
	
	public int AddToQueue(String name) {
		this.queue.add(name);
		return this.queue.indexOf(name) + 1;
	}
	
	public String HelpCustomer() {
		String name1 = this.queue.get(0);
		this.queue.remove(0);
		return name1;
	}
	
	
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		DeliCounter queue1 = new DeliCounter();
		
		boolean flag = true;
		do {
	
			System.out.println("Choose an option: ");
			System.out.println("1. Add customer to queue");
			System.out.println("2. Help customer");
			System.out.println("3. Exit program");
				
			System.out.print("Choice: ");
			String choice = input.nextLine();
			
			switch (choice) {
				case "1": 	// Add Customer
					System.out.print("Name of customer to add to queue: ");
					String name = input.nextLine();
					
					System.out.println("Position in queue: " + queue1.AddToQueue(name));
					break;
				case "2":  // Help customer
					try {
						System.out.println(queue1.HelpCustomer() + " has been served.");
					} catch (IndexOutOfBoundsException e) {
						System.out.println("No customers to help.");
					}
					break;
				case "3": // Exit
					flag = false;
					System.out.println("Closing...");
					break;
				default:
					System.out.println("Not an option, try again.");
			}
			
		} while (flag);
		
		input.close();
	}

}
