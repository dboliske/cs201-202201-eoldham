package labs.lab1;

import java.util.Scanner;

public class Question12to16 {
	public static void main(String[] args) {
		Scanner input = new Scanner (System.in); // Scanner for user input
		
		System.out.print("Length in inches: ");
		double length = input.nextDouble();

		System.out.print("Width in inches: ");
		double width = input.nextDouble();
		
		System.out.print("Height in inches: ");
		double height = input.nextDouble();
		
		System.out.println("Surface area in square feet: " + ((length*width)+(length*height)+(width*height))/72);
	}	
}
