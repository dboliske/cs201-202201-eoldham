package labs.lab1;

public class Question2to6 {
	public static void main(String[] args) {
		
		int myAge = 19;
		int dadAge = 49;
		int myBirthYear = 2002;
		double myHeightIn = 67;
		int myHeightInInt = (int)myHeightIn;
		int myHeightFt = (int)(java.lang.Math.floor(myHeightIn/12)); // Had to search for the floor function
		
		System.out.println("My Age: " + myAge);
		System.out.println("My Dad's Age: " + dadAge);
		System.out.println("3) My age subtracted from my father's age = " + (dadAge - myAge));
		System.out.println(" ");
		System.out.println("My birth year: " + myBirthYear);
		System.out.println("4) My birth year multiplied by 2 = " + (myBirthYear*2));
		System.out.println(" ");
		System.out.println("My height in inches: " + myHeightInInt);
		System.out.println("5) My height in centimeters = " + (myHeightIn*2.54));
		System.out.print("6) My height in feet and inches = " + myHeightFt + "ft. and ");
		System.out.println(myHeightInInt%12 + "in.");
	}
}
