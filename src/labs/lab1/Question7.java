package labs.lab1;

import java.util.Scanner;

public class Question7 {
	public static void main(String[] args) {
		Scanner input = new Scanner (System.in); // Scanner for user input
		
		System.out.print("First Name: ");
		String firstName = input.nextLine();
		System.out.println("The first initial is: " + firstName.charAt(0));
	}
}
