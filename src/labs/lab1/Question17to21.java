package labs.lab1;

import java.util.Scanner;

public class Question17to21 {
	public static void main(String[] args) {
		Scanner input = new Scanner (System.in); // Scanner for user input
		
		System.out.print("Inches: ");
		double inches = input.nextDouble();
		System.out.println("Length in centimeters = " + (inches*2.54));
	}
}
