package labs.lab1;

import java.util.Scanner;

public class Question8to11 {
	public static void main(String[] args) {
		Scanner input = new Scanner (System.in); // Scanner for user input
		
		System.out.print("Temperature in Fahrenheit: ");
		double tempF = input.nextDouble();
		System.out.print("This temp from Fahrenheit to Celsius is: " + ((tempF - 32)*5/9));
		
		System.out.println("");
		System.out.print("Temperature in Celsius: ");
		double tempC = input.nextDouble();
		System.out.print("This temp from Celsius to Fahrenheit is: " + (tempC*9/5 + 32));
	}
}
