package labs.lab5;

import java.io.File;
import java.util.Scanner;

public class CTAStopApp {
	
		public static CTAStation[] readFile(String filename) {
			CTAStation[] Stations = new CTAStation[34];
			int count = 0;
			
			try {
				File f = new File(filename);
				Scanner input = new Scanner(f);
				
				while (input.hasNextLine()) {
					try {
						
						String line = input.nextLine();
						String[] values = line.split(",");
						
						CTAStation c = new CTAStation(values[0], Double.parseDouble(values[1]), Double.parseDouble(values[2]), 
								values[3], Boolean.parseBoolean(values[4]), Boolean.parseBoolean(values[5]));
						
						Stations[count] = c;
						count++;
						
						
					} catch (Exception e) {
						System.out.println("Error reading next line");
					}
				}
				
				input.close();
			} catch (Exception e) {
				System.out.println("Error reading file");
			}
			
			return Stations;
		}
		
		
		public static void menu(CTAStation[] data) {
			Scanner input = new Scanner(System.in);
			boolean done = false;
			
			do {
				System.out.println("1. Display Station Names");
				System.out.println("2. Display Stations with/without Wheelchair Access");
				System.out.println("3. Display Nearest Station");
				System.out.println("4. Exit");
				System.out.print("Choice: ");
				String choice = input.nextLine();
					
					// menu
				switch (choice) {
					case "1":
						for (int i=0; i<data.length; i++) {
							System.out.println((i+1) + ". " + data[i].getName());
						}
						break;
					case "2":
						boolean flag = true;
						
						do {
							System.out.print("Wheelchair accessible? (y/n): ");
							char YesNo = input.nextLine().charAt(0);
								
							if ((YesNo != 'y') && (YesNo != 'n') && (YesNo != 'Y') && (YesNo != 'N')) {
								System.out.println("This was not a valid response: " + YesNo);
							} else {
								
								if ((YesNo == 'y') || (YesNo == 'Y')) {
									for (int i=0; i<data.length; i++) {
										if (data[i].hasWheelchair() == true) {
											System.out.println(data[i].getName());
										}
									}
								} else {
									for (int i=0; i<data.length; i++) {
										if (data[i].hasWheelchair() == false) {
											System.out.println(data[i].getName());
										}
									}
								}
								
								flag = false;
							}		
						} while (flag);
						
						break;
					case "3":
						System.out.print("Enter latitude: ");
						double lat = input.nextDouble();
						
						System.out.print("Enter longitude: ");
						double lon = input.nextDouble();
						
						double min = data[0].getGeoLocation().calcDistance(lat, lon);
						int minIndex = 0;
						for (int i = 0; i<data.length; i++) {
							if (data[i].getGeoLocation().calcDistance(lat, lon) < min) {
								min = data[i].getGeoLocation().calcDistance(lat, lon);
								minIndex = i;
							}
						}
						
						System.out.println("Nearest Station is: " + data[minIndex].getName());
						
						break;
					case "4":
							done = true;
							break;
					default:
							System.out.println("I'm sorry, but I can't do that.");
					}
					
				} while (!done);
				
				input.close();
		}
		
		
		public static void main(String[] args) {
			CTAStation[] data = readFile("C:\\Users\\zekeo\\git\\cs201-202201-eoldham\\src\\labs\\lab5\\CTAStops.csv"); // I could only get it to work with this filename
			menu(data);
			
			System.out.println("Program ending...");
		}
}
