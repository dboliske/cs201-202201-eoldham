package labs.lab5;

import labs.lab5.GeoLocation2;

public class GeoLocation2 {
	
	private double lat;
	private double lng;

	public GeoLocation2() {
		lat = 0;
		lng = 0;
	}

	public GeoLocation2(double latitude, double longitude) {
		lat = latitude;
		lng = longitude;
	}
	
	public double getLat() {
		return lat;
	}
	
	public double getLng() {
		return lng;
	}
	
	public void setLat(double lat) {
		this.lat = lat;
	}
	
	public void setLng(double lng) {
		this.lng = lng;
	}
	
	public String toString() {
		return lat + ", " + lng;
	}
		
	public boolean validLat() {
		if (getLat()<90 && getLat()>-90) {
			return true;
		} else {
			return false;
		}
	}
	
	public boolean validLng() {
		if (getLng()<180 && getLng()>-180) {
			return true;
		} else {
			return false;
		}
	}
	
	public boolean equals(GeoLocation2 b) {
		if (this.lat != b.getLat()) {
			return false;
		} else if (this.lng != b.getLng()) {
			return false;
		} else {
			return true;
		}
	}
	
	public double calcDistance(GeoLocation2 b) {
		return Math.sqrt(Math.pow(this.lat - b.getLat(), 2) + Math.pow(this.lng - b.getLng(), 2));
	}
	
	public double calcDistance(double lat, double lng) {
		return Math.sqrt(Math.pow(this.lat - lat, 2) + Math.pow(this.lng - lng, 2));
	}
}