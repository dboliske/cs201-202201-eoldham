package labs.lab5;

public class CTAStation {
	
	private String name;
	private String location;
	private boolean wheelchair;
	private boolean open;
	private GeoLocation2 GeoLocation;
	
	
	
	public CTAStation() {
		name = "35th Bronzeville / IIT";
		location = "elevated";
		wheelchair = true;
		open = true;
		GeoLocation = new GeoLocation2(41.83168, -87.6258);
	}
	
	public CTAStation(String N, double lat, double lng, String loc, boolean op, boolean wheel) {
		name = N;
		location = loc;
		wheelchair = wheel;
		open = op;
		GeoLocation = new GeoLocation2(lat, lng);
	}
	
	
	
	public String getName() {
		return name;
	}
	
	public String getLocation() {
		return location;
	}
	
	public boolean hasWheelchair() {
		return wheelchair;
	}
	
	public boolean isOpen() {
		return open;
	}
	
	public GeoLocation2 getGeoLocation() {
		return GeoLocation;
	}
	
	
	
	public void setLocation(String location) {
		this.location = location;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setOpen(boolean open) {
		this.open = open;
	}
	
	public void setWheelchair(boolean wheelchair) {
		this.wheelchair = wheelchair;
	}
	
	public void setGeoLocation(GeoLocation2 g) {
		this.GeoLocation = g;
	}
	
	
	
	public String toString() {
		return "Name: " + this.name
				+ "Location: " + this.location
				+ "Wheelchair: " + this.wheelchair
				+ "Open: " + this.open
				+ "GeoLocation: " + this.GeoLocation.toString();
	}
	
	
	
	public boolean equals(CTAStation c) {
		if (this.name != c.getName()) {
			return false;
		} else if (this.location != c.getLocation()) {
			return false;
		} else if (this.open != c.isOpen()) {
			return false;
		} else if (this.wheelchair != c.hasWheelchair()) {
			return false;
		} else if (!this.GeoLocation.equals(c.GeoLocation)) {
			return false;
		} else {
			return true;
		}
	}
	
}
