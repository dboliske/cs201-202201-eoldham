package labs.lab4;

public class Potion {

	private String name;
	private double strength;
	
	public Potion() {
		name = "OUCH";
		strength = 10;
	}
	
	public Potion(String NA, double ST) {
		name = NA;
		strength = ST;
	}
	
	public String getName() {
		return name;
	}
	
	public double getStrength() {
		return strength;
	}
	
	public void setName(String NA) {
		this.name = NA;
	}
	
	public void setStrength(double ST) {
		this.strength = ST;
	}
	
	public String toString() {
		return "Name: " + name + ", Strength: " + String.valueOf(strength);
	}
	
	public boolean validStrength() {
		if (getStrength() < 10 && getStrength() > 0) {
			return true;
		} else {
			return false;
		}
	}
	
	public boolean equals(Potion PO) {
		if (this.name.compareToIgnoreCase(PO.name) != 0) {
			return false;
		} else if (this.strength != PO.strength) {
			return false;
		} else {
			return true;
		}
	}
	
	public static void main(String[] args) {
		Potion potion1 = new Potion();
		Potion potion2 = new Potion("JavaJuice", 9);
		
		System.out.println(potion1.toString());
		System.out.println(potion2.toString());
	}
}
