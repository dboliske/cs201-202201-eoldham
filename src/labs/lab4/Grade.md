# Lab 4

## Total

27/30

## Break Down

* Part I GeoLocation
  * Exercise 1-9        8/8
  * Application Class   0/1
* Part II PhoneNumber
  * Exercise 1-9        8/8
  * Application Class   0/1
* Part III 
  * Exercise 1-8        8/8
  * Application Class   0/1
* Documentation         3/3

## Comments
- No application classes