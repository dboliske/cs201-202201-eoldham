package labs.lab4;

public class GeoLocation {
	
	private double lat;
	private double lng;
	
	public GeoLocation() {
		lat = 0;
		lng = 0;
	}

	public GeoLocation(double latitude, double longitude) {
		lat = latitude;
		lng = longitude;
	}
	
	public double getLat() {
		return lat;
	}
	
	public double getLng() {
		return lng;
	}
	
	public void setLat(double lat) {
		this.lat = lat;
	}
	
	public void setLng(double lng) {
		this.lng = lng;
	}
	
	public String toString() {
		return lat + ", " + lng;
	}
		
	public boolean validLat() {
		if (getLat()<90 && getLat()>-90) {
			return true;
		} else {
			return false;
		}
	}
	
	public boolean validLng() {
		if (getLng()<180 && getLng()>-180) {
			return true;
		} else {
			return false;
		}
	}
	
	public boolean equals(GeoLocation b) {
		if (this.lat != b.getLat()) {
			return false;
		} else if (this.lng != b.getLng()) {
			return false;
		} else {
			return true;
		}
	}
	
	public static void main(String[] args) {
		GeoLocation var1 = new GeoLocation();
		GeoLocation var2 = new GeoLocation(1,1);
		
		System.out.println("Lat 1 = " + var1.getLat());
		System.out.println("Lng 1 = " + var1.getLng());
		
		System.out.println("Lat 2 = " + var2.getLat());
		System.out.println("Lng 2 = " + var2.getLng()); 
	}
}
