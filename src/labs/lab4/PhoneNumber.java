package labs.lab4;

public class PhoneNumber {
	
	private String countryCode;
	private String areaCode;
	private String number;
	
	public PhoneNumber() {
		countryCode = "1";
		areaCode = "312";
		number = "1111111";
	}
	
	public PhoneNumber(String CC, String AC, String NU) {
		countryCode = CC;
		areaCode = AC;
		number = NU;
	}
	
	public String getCountryCode() {
		return countryCode;
	}
	
	public String getAreaCode() {
		return areaCode;
	}
	
	public String getNumber() {
		return number;
	}
	
	public void setCountryCode(String CC) {
		this.countryCode = CC;
	}
	
	public void setAreaCode(String AC) {
		this.areaCode = AC;
	}
	
	public void setNumber(String NU) {
		this.number = NU;
	}
	
	public String toString() {
		return countryCode + " (" + areaCode + ") - " + number;
	}
	
	public boolean validAreaCode() {
		if (getAreaCode().length() == 3) {
			return true;
		} else {
			return false;
		}
	}
	
	public boolean validNumber() {
		if (getNumber().length() == 7) {
			return true;
		} else {
			return false;
		}
	}
	
	public boolean equals(PhoneNumber PN) {
		if (this.countryCode != PN.getCountryCode()) {
			return false;
		} else if (this.areaCode != PN.getAreaCode()) {
			return false;
		} else if (this.number != PN.getNumber()) {
			return false;
		} else {
			return true;
		}
	}
}

