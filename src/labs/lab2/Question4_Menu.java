package labs.lab2;

import java.util.Scanner;

public class Question4_Menu {
	public static void main(String[] args) {
		Scanner input = new Scanner (System.in); // Scanner for user input
		boolean flag = true;
		
		while (flag) {
			// Menu options
			System.out.println("1) Say Hello.");
			System.out.println("2) Sum two numbers.");
			System.out.println("3) Multiply two numbers.");
			System.out.println("4) Exit Program.");
			System.out.print("Number of Choice: ");
			int choice = input.nextInt();
			
			switch (choice) {
				case 1: // Say Hello
					System.out.println("Hello.");
					break;
				case 2: // Sum
					System.out.print("First Number: ");
					double sum1 = input.nextDouble();
					System.out.print("Second Number: ");
					double sum2 = input.nextDouble();
					
					System.out.println("Sum = " + (sum1+sum2));
					break;
				case 3: // Product
					System.out.print("First Number: ");
					double product1 = input.nextDouble();
					System.out.print("Second Number: ");
					double product2 = input.nextDouble();
					
					System.out.println("Product = " + (product1*product2));
					break;
				case 4: // Exit
					flag = false;
					break;
				default: // Else
					System.out.println("Not an option.");
			}
		}
		
		input.close();
		System.out.println("Farewell");
	}
}
