package labs.lab2;

import java.util.Scanner;

public class Question1to3_ExamGrades {
	public static void main(String[] args) {
		Scanner input = new Scanner (System.in);
		
		double sumOfGrades = 0;
		double numOfGrades = 0;
		double currentGrade = 0;
		double averageGrade = 0;
		
		while (currentGrade != -1) {		
			System.out.print("Enter a grade. (If there are no more grades to enter, enter -1): ");
			currentGrade = input.nextDouble();
			
			if (currentGrade != -1) { // prevents computing -1 as a grade
				sumOfGrades = sumOfGrades + currentGrade; // sums the grades entered
				numOfGrades = numOfGrades + 1; // counts the number of grades entered
			}
			else {
				break;
			}
		}
		
		
		if (numOfGrades == 0) {
			System.out.println("No grades entered.");	// prevents 0/0
		}
		else {
			averageGrade = (sumOfGrades/numOfGrades);
			System.out.println("Average grade: " + averageGrade);
		}
		
		input.close();
	}

}
