package labs.lab2;

import java.util.Scanner;

public class Question1_Square {
	public static void main(String[] args) {
		Scanner input = new Scanner (System.in); // Scanner for user input
		
		System.out.print("Enter positive integer length of square: ");
		int length = (int) input.nextDouble();
		
		int counter1 = 0;
		int counter2 = 0;
		
		while (counter2 < length) { // repeats the inside loop until there are n lines of n stars, where n = length
			
			while (counter1 < length) { // prints out one line of *'s
				System.out.print("* ");
				counter1 = counter1 + 1;
			}
			
			System.out.println(""); // goes to next line
			counter2 = counter2 + 1;
			counter1 = 0; // resets counter before entering the inside loop, so it will keep printing *'s
		}
		
		input.close();
	}
}
