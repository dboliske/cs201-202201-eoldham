package labs.lab3;

public class Question3_Min {
	public static void main(String[] args) {
		double[] data = {72, 101, 108, 108, 111, 32, 101, 118, 101, 114, 121, 111, 110, 101, 33, 32, 76, 111, 111, 107, 32, 97, 116, 32, 116, 104, 101, 115, 101, 32, 99, 111, 111, 108, 32, 115, 121, 109, 98, 111, 108, 115, 58, 32, 63264, 32, 945, 32, 8747, 32, 8899, 32, 62421};
		
		double min = data[0]; 
		double current = data[0];
		
		for (int index=1; index < data.length; index++) {
			current = data[index];
			if (current < min) { // sets the minimum to the current value if the current value is less the the expected minimum
				min = current;
			}
		}
		
		System.out.println("The minimum = " + min);
	}
}
