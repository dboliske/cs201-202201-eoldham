package labs.lab3;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class Question1_Grades {
	public static void main(String[] args) throws IOException {
		File file = new File("src/labs/lab3/grades.csv");
		Scanner input = new Scanner(file);
		
		double sum = 0;
		int counter = 0;
		
		while (input.hasNextLine()) {
			String line = input.nextLine();
			String currentString = line.substring(line.indexOf(',')+1); // isolates grade
			double currentDouble = Double.parseDouble(currentString); // converts grade from string to double
			sum = sum + currentDouble; // sums grades
			counter++;	// counts number of grades
		}
		
		double averageGrade = (sum/counter); // computes average
		System.out.println("Average grade = " + averageGrade);
		
		input.close();
	}
}