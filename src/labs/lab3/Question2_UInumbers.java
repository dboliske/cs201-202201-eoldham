package labs.lab3;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.Scanner;

public class Question2_UInumbers {
	public static void main(String[] args) throws IOException {
		// Code below will correctly save all numbers to an array until the user inputs "done" - just need to write to a file
		
		
		Scanner input = new Scanner(System.in);
		
		String numberCurrent = "0";
		double[] array = new double[1];
		int counter = 0;
		
		while (numberCurrent.compareToIgnoreCase("done") < 0) {
			System.out.print("Input a number, if there are no more numbers to input, input 'done': ");
			numberCurrent = input.nextLine();
			
			if (numberCurrent.compareToIgnoreCase("done") < 0) {
				array[counter] = Double.parseDouble(numberCurrent); //sets the array at index 'counter' to have new value

				double[] arrayCopy = new double[array.length+1]; // copies the array with extended length
				for (int index=0 ; index<=counter; index++) {
					arrayCopy[index] = array[index];
				}
				
				array = arrayCopy; // sets the array to have extended length
				counter++; // adds 1 to the counter
			} else if (array.length == 1) { // user must have not entered any numbers
				array = null;
			} else { // array has extended length, needs to be trimmed
				double[] arrayCopy = new double[array.length-1];
				for (int index=0 ; index<(array.length-1); index++) {
					arrayCopy[index] = array[index];
				}
				array = arrayCopy;
			}
		}
		
		
		
		// writing in the file...
		try {
		System.out.print("Enter file location: "); // I made a file named Question2Text.txt for testing, it should be in the labs.lab3 package
		String fileLocation = input.nextLine();
		
		String arrayString = Arrays.toString(array);
		
		FileWriter fWriter = new FileWriter(fileLocation);
		
		fWriter.write(arrayString);
		System.out.println("Data stored in file: " + arrayString);
		
		fWriter.close();
		} catch (IOException e) {
			System.out.print("IOException");
		}
		
		input.close();
	}

}
