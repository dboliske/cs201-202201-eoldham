package labs.lab7;

public class BubbleSort {

	public static double[] bubSort(double[] array) {
		boolean hasSwapped = true;
		
		do {
			hasSwapped = false;
			for (int i = 0; i < array.length-1; i++) {
				if (array[i] > array[i+1]) {
					double temp = array[i];
					array[i] = array[i+1];
					array[i+1] = temp;
					
					hasSwapped = true;
				}
			}
			
		} while (hasSwapped);
		
		return array;
	}
	
	public static void main(String[] args) {
		double[] array = {10, 4, 7, 3, 8, 6, 1, 2, 5, 9};
		
		double[] SortedArray = bubSort(array);
		
		System.out.print("{");
		for (int i = 0; i <= SortedArray.length -1; i++) {
			if (i == SortedArray.length-1) {
				System.out.print(SortedArray[i]);
			} else {
				System.out.print(SortedArray[i] + ", ");
			}
		}
		System.out.print("}");
	}
}
