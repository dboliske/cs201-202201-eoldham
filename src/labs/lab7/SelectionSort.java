package labs.lab7;

public class SelectionSort {

	public static double[] selSort(double[] array) {
		
		for (int j=0; j < array.length; j++) {
			
			// find min
			int min = j;
			for (int i = j; i < array.length; i++) {
				if (array[i] < array[min]) {
					min = i;
				}
			}
			
			// swap min to correct place
			double temp = array[min];
			array[min] = array[j];
			array[j] = temp;
		}
		
		return array;
	}
	
	public static void main(String[] args) {
		double[] array = {3.142, 2.718, 1.414, 1.732, 1.202, 1.618, 0.577, 1.304, 2.685, 1.282};
		
		double[] SortedArray = selSort(array);
		
		System.out.print("{");
		for (int i = 0; i <= SortedArray.length -1; i++) {
			if (i == SortedArray.length-1) {
				System.out.print(SortedArray[i]);
			} else {
				System.out.print(SortedArray[i] + ", ");
			}
		}
		System.out.print("}");
	}
}
