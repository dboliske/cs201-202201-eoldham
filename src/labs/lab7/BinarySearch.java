package labs.lab7;

import java.util.Scanner;

public class BinarySearch {

	public static int binarySearch(String[] array, String value) {
		int start = 0;
		int end = array.length;
		int pos = -1;
		boolean found = false;
		
		while (!found && start != end) {
			int middle = (start + end) / 2;
			
			if (array[middle].equalsIgnoreCase(value)) {
				found = true;
				pos = middle;
			} else if (array[middle].compareToIgnoreCase(value) < 0) {
				start = middle + 1;
			} else {
				end = middle;
			}
		}
		
		return pos;
	}
	
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		String[] array = {"c", "html", "java", "python", "ruby", "scala"};
		
		System.out.print("Enter word to search for: ");
		String word = input.nextLine();
		
		int index = binarySearch(array, word);
		
		if (index == -1) {
			System.out.println("Word not found.");
		} else {
			System.out.println("Word at index: " + index);
		}
		
		input.close();
	}
}
