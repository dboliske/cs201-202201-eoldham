package labs.lab7;

public class InsertionSort {
	
	public static String[] insSort(String[] array) {
		for (int j=1; j<array.length; j++) {
			int i = j;
			while (i > 0 && array[i].compareTo(array[i-1]) < 0) {
				String temp = array[i];
				array[i] = array[i-1];
				array[i-1] = temp;
				i--;
			}
		}
		
		return array;
	}
	
	public static void main(String[] args) {
		String[] array = {"cat", "fat", "dog", "apple", "bat", "egg"};
		
		String[] SortedArray = insSort(array);
		
		System.out.print("{");
		for (int i = 0; i <= SortedArray.length -1; i++) {
			if (i == SortedArray.length-1) {
				System.out.print(SortedArray[i]);
			} else {
				System.out.print(SortedArray[i] + ", ");
			}
		}
		System.out.print("}");
	}
}
