package exams.first;
import java.util.Scanner;

public class MidtermQuestion1 {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in); // Scanner for user input
		
		System.out.print("Enter an integer: ");  // prompt user
		int var = input.nextInt(); // set var
		
		var = var + 65; // add 65
		
		var = (char) var; // convert to char
		
		System.out.println(var); // display var
		
		input.close();
	}
}
