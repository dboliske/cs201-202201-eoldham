# Midterm Exam

## Total

95/100

## Break Down

1. Data Types:                  20/20
    - Compiles:                 5/5
    - Input:                    5/5
    - Data Types:               5/5
    - Results:                  5/5
2. Selection:                   20/20
    - Compiles:                 5/5
    - Selection Structure:      10/10
    - Results:                  5/5
3. Repetition:                  20/20
    - Compiles:                 5/5
    - Repetition Structure:     5/5
    - Returns:                  5/5
    - Formatting:               5/5
4. Arrays:                      20/20
    - Compiles:                 5/5
    - Array:                    5/5
    - Exit:                     5/5
    - Results:                  5/5
5. Objects:                     15/20
    - Variables:                4/5
    - Constructors:             3/5
    - Accessors and Mutators:   5/5
    - toString and equals:      3/5

## Comments

1. Good
2. Good
3. Good
4. Good
5. Class is not named correctly, non-default constructor does not validate `age` before setting it, and `equals` method does not follow UML diagram nor check `name` correctly.
