package exams.first;

import java.util.Scanner;

public class MidtermQuestion4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner input = new Scanner(System.in);
		
		String[] array = new String[5];
		
		for (int i = 0; i<5; i++) {
			System.out.print("Enter a word: ");
			String current = input.nextLine();
			
			array[i] = current;
		}
		
		for (int i = 0; i<5; i++) {
			for (int j = 0; j<5; j++) {
				if (j != i) {
					int same = array[i].compareTo(array[j]);
					
					if (same == 0) {
						System.out.println(array[i]);
					}
				}
			}
		}
		
		input.close();
	}

}
