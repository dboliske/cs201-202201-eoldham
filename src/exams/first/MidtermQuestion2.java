package exams.first;

import java.util.Scanner;

public class MidtermQuestion2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner input = new Scanner(System.in);
		
		System.out.print("Enter an integer: ");
		int var = input.nextInt();
		
		if ((var%2 == 0) && (var%3 == 0)) {
			System.out.println("foobar");
		} else if (var%2 == 0) {
			System.out.println("foo");
		} else if (var%3 == 0) {
			System.out.println("bar");
		}
		
		input.close();
	}
}
