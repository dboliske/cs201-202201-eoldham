package exams.first;

public class MidtermQuestion5 {

	private String name;
	private int age;
	
	public void Pet() {
		name = "Spot";
		age = 3;
	}
	
	public void Pet(String n, int a) {
		name = n;
		age = a;
	}
	
	public void setName(String n) {
		this.name = n;
	}
	
	public void setAge(int a) {
		if (a > 0) {
			this.age = a;
		}
	}
	
	public String getName() {
		return this.name;
	}
	
	public int getAge() {
		return this.age;
	}
	
	public boolean equals(MidtermQuestion5 p) {
		if (this.age != p.getAge()) {
			return false;
		} else if (this.name != p.getName()) {
			return false;
		} else {
			return true;
		}
	}
	
	public String toString() {
		return "Name: " + this.name
				+ "Age: " + this.age;
	}
}
