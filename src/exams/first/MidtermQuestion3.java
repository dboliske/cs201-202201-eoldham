package exams.first;

import java.util.Scanner;

public class MidtermQuestion3 {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		System.out.print("Enter a positive integer: ");
		int var = input.nextInt();
		
		for (int i = 1; i<=var; i++) {
			for (int j = 1; j<=i; j++) {
				System.out.print("* ");
			}
			System.out.println();
		}
		
		input.close();
	}
}
