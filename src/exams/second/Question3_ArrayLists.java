package exams.second;

import java.util.ArrayList;
import java.util.Scanner;

public class Question3_ArrayLists {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		ArrayList<Double> numArray = new ArrayList<Double>(0);
		double num = 0;
		boolean flag = false;
		boolean hasParsed = true;
		
		do {
			System.out.print("Enter a number, or enter 'done' to exit: ");
			String choice = input.nextLine().toLowerCase();
			
			switch (choice) {
			case "done":
				flag = true;
				
				if (numArray.size() == 0) {
					System.out.println("No numbers entered");
				} else {
					double min = numArray.get(0);
					double max = numArray.get(0);
					
					for (int i = 1; i < numArray.size(); i++) {
						if (min > numArray.get(i)) {
							min = numArray.get(i);
						} else if (max < numArray.get(i)) {
							max = numArray.get(i);
						}
					}
					System.out.println("Min = " + min);
					System.out.println("Max = " + max);
				}
				
				break;
			default:
				try {
					num = Double.parseDouble(choice);
				} catch (NumberFormatException e) {
					System.out.println(choice + " is not a recognized input");
					hasParsed = false;
				}
				
				if (hasParsed) {
					numArray.add(num);
				}
			}
			
		} while (!flag);
		
		input.close();
	}

}
