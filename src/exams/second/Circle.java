package exams.second;

// Question 2
public class Circle extends Polygon {
	
	private double radius;
	
	public Circle() {
		this.name = "Circle";
		this.radius = 1;
	}
	
	public void setRadius(double r) {
		if (r > 0) {
			this.radius = r;
		}
	}
	
	public double getRadius() {
		return this.radius;
	}
	
	public double area() {
		return Math.PI*this.radius*this.radius;
	}
	
	public double perimiter() {
		return 2*Math.PI*this.radius;
	}
	
	public String toString() {
		return this.name + ", Radius: " + this.radius;
	}
}
