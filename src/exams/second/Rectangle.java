package exams.second;

// Question 2
public class Rectangle extends Polygon {
	
	private double width;
	private double height;
	
	public Rectangle() {
		this.width = 1;
		this.height = 1;
		this.name = "Rectangle";
	}
	
	public void setWidth(double w) {
		if (w > 0) {
			this.width = w;
		}
	}
	
	public void setHeight(double h) {
		if (h > 0) {
			this.height = h;
		}
	}
	
	public double getWidth() {
		return this.width;
	}
	
	public double getHeight() {
		return this.height; //
	}
	
	public double area() {
		return this.width*this.height;
	}
	
	public double perimiter() {
		return 2*(this.width + this.height);
	}
	
	public String toString() {
		return this.name + ", Width: " + this.width + ", Height: " + this.height;
	}
}