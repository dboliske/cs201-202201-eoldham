package exams.second;

import java.util.Scanner;

public class Question5_Searching {
	
	public static int search(double[] array, double num) {
		int step = (int) Math.sqrt(array.length);
		int prev = 0;
		
		if (array[prev] == num) {
			return prev;
		}
		
		while (array[Math.min(step, array.length - 1)] < num) {
			prev = step;
			step += (int)Math.sqrt(array.length);
			if (prev >= array.length) {
				return -1;
			}
		}
		
		do {
			if (prev == Math.min(step, array.length - 1)) {
				return -1;
			}
			prev++;
		} while (array[prev] < num); 
		
		if (array[prev] == num) {
			return prev;
		}
		
		return -1;
	}
	
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		double[] array = {0.577, 1.202, 1.282, 1.304, 1.414, 1.618, 1.732, 2.685, 2.718, 3.142};
		double num = 0;
		boolean flag = false;
		boolean hasParsed = true;
		
		do {
			System.out.print("Enter a number: ");
			String numStr = input.nextLine();
			
			try {
				num = Double.parseDouble(numStr);
				hasParsed = true;
			} catch (NumberFormatException e) {
				System.out.println(numStr + " is not a valid input");
				hasParsed = false;
			}
			
			if (hasParsed) {
				flag = true;
			}
		} while (!flag);
		
		
		int pos = search(array, num);
		
		System.out.println("Index: " + pos);
		
		input.close();
	}
}
