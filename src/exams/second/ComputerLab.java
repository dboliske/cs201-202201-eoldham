package exams.second;

// Quesiton 1
public class ComputerLab extends Classroom {
	private boolean computers;
	
	public void setComputers(boolean c) {
		this.computers = c;
	}
	
	public boolean getComputers() {
		return this.computers;
	}
	
	@Override
	public String toString() {
		if (this.getComputers()) {
			return super.toString() + "Has computers: True";
		}
		return super.toString() + "Has computers: False";
	}
}
