package exams.second;

// Question 1
public class Classroom {
	
	private String building;
	private String roomNumber;
	private int seats;
	
	public void Classroom() {
		this.building = "Stuart";
		this.roomNumber = "112";
		this.seats = 30;
	}
	
	public void setBuilding(String b) {
		this.building = b;
	}
	
	public void setRoomNumber(String r) {
		this.roomNumber = r;
	}
	
	public void setSeats(int s) {
		if (s > 0) {
			this.seats = s;
		}
	}
	
	public String getBuilding() {
		return this.building;
	}
	
	public String getRoomNumber() {
		return this.roomNumber;
	}
	
	public int getSeats() {
		return seats;
	}
	
	public String toString() {
		return "Building: " + this.building +
				", Room: " + this.roomNumber +
				", Seats: " + this.seats;
	}
	
}
