package exams.second;

public class Question4_Sorting {

	public static void main(String[] args) {
		String[] array = {"speaker", "poem", "passenger", "tale", "reflection", "leader", "quality", "percentage", "height", "wealth", "resource", "lake", "importance"};
		
		for (int i=0; i<array.length-1; i++) {
			int min = i; // set min
			
			for (int j = i+1; j<array.length; j++) { 
				if (array[j].compareTo(array[min])<0) {
					min = j; // find min
				}
			}
			
			if (min!=i) { // if min isn't already at i, then swap i and min
				String tempStr = array[i];
				array[i] = array[min];
				array[min] = tempStr;
			}
		}
		
		for (int i=0; i<array.length; i++) {
			if (i!=array.length-1) {
				System.out.print(array[i] + ", ");
			} else {
				System.out.print(array[i]);
			}
		}

	}

}
