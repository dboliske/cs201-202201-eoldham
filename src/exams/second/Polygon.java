package exams.second;

// Question 2
abstract class Polygon {

	protected String name;
	public abstract double area();
	public abstract double perimiter();
	public abstract String toString();
	
	public Polygon() {
	}
	
	public void setName(String n) {
		this.name = n;
	}
	
	public String getName() {
		return this.name;
	}
}